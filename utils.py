import requests
import json
import os

from sampling import ParamSampler
from parsing import ModelFactory


def calibrate(search_space, benchmark_url, metrics=[]):
    
    param_sampler = ParamSampler('upper')
    config = param_sampler.sample(search_space)
    model_factory = ModelFactory()
    model = model_factory.create_model_from_config(
        config['network_architecture'],
        config['training_params']['input_shape'],
        config['metadata']['model_name']
    )
    model.save('tmp_model_upper.h5')

    ret = requests.post(
        benchmark_url, 
        files={'model': open('tmp_model_upper.h5', 'rb')}, 
        timeout=None,
        verify=False
    )
    if ret.status_code == 200:
        results = ret.json()
        return {metric: float(results[metric]) for metric in metrics}
    else:
        print(ret.text)
    os.remove('tmp_model_upper.h5')
        

def call_benchmark(model, benchmark_url, metrics=[]):
    
    model.save('tmp_model.h5')

    ret = requests.post(
        benchmark_url, 
        files={'model': open('tmp_model.h5', 'rb')}, 
        timeout=None,
        verify=False
    )
    if ret.status_code == 200:
        results = ret.json()
        return {metric: float(results[metric]) for metric in metrics}
    else:
        print(ret.text)
    os.remove('tmp_model.h5')