import random
from collections import Counter


class ParamSampler:
    
    def __init__(self, sampling_strategy):
        self.sampling_strategy = sampling_strategy
    

    def _get_sampling_fn(self, trial=None):

        if self.sampling_strategy == 'optuna':
            # currying to pass the optuna trial object
            def sampling_fn(allowed_values, param_name):
                if type(allowed_values) == list:
                    return trial.suggest_categorical(param_name, allowed_values)
                elif type(allowed_values) == dict:
                    a, b = allowed_values['range']
                    if allowed_values['dtype'] == 'int':
                        return trial.suggest_int(param_name, a, b)
                    elif allowed_values['dtype'] == 'float':
                        return trial.suggest_float(param_name, a, b)
                    else:
                        raise Exception(f'Unsupported dtype of allowed value range: {allowed_values["dtype"]}')
                else:
                    raise Exception(f'Unsupported format of allowed values: {allowed_values}')
            
        elif self.sampling_strategy == 'random':
            # support the same function format
            def sampling_fn(allowed_values, param_name=None):
                if type(allowed_values) == list:
                    return random.choice(allowed_values)
                elif type(allowed_values) == dict:
                    a, b = allowed_values['range']
                    if allowed_values['dtype'] == 'int':
                        return random.randint(a, b)
                    elif allowed_values['dtype'] == 'float':
                        return random.uniform(a, b)
                    else:
                        raise Exception(f'Unsupported dtype of allowed value range: {allowed_values["dtype"]}')
                else:
                    raise Exception(f'Unsupported format of allowed values: {allowed_values}')
        
        elif self.sampling_strategy == 'upper':
            # utility for getting the largest possible model from config
            # ! completely deterministic !
            def sampling_fn(allowed_values, param_name=None):
                if type(allowed_values) == list:
                    return allowed_values[-1]
                elif type(allowed_values) == dict:
                    return allowed_values['range'][-1]
                else:
                    raise Exception(f'Unsupported format of allowed values: {allowed_values}')

        else:
            raise Exception(f"Unsupported sampling strategy: {self.sampling_strategy}")

        return sampling_fn


    def sample(self, search_space, trial=None):
        
        sampling_fn = self._get_sampling_fn(trial=trial)

        config = {
            "metadata": search_space['metadata'],
            "training_params": {},
            "network_architecture": []
        }

        for key in search_space['training_params']:
            
            if key == 'input_shape':
                if type(search_space['training_params'][key][0]) == list:
                    config['training_params'][key] = sampling_fn(
                        search_space['training_params'][key], param_name=f'training_{key}'
                    )
                else:
                    config['training_params'][key] = search_space['training_params'][key]
            
            elif type(search_space['training_params'][key]) in [list, dict]:
                config['training_params'][key] = sampling_fn(
                    search_space['training_params'][key], param_name=f'training_{key}'
                )
            
            else:
                config['training_params'][key] = search_space['training_params'][key]

        self.constraints = []
        config['network_architecture'] = [
            self.sample_subblock_params(
                subblock,
                sampling_fn
            ) for subblock in search_space['network_architecture']
        ]

        return config


    def _check_constraints(self, subblock_name, param_name):
        for constraint in self.constraints:
            if constraint['layer'] in subblock_name and constraint['param'] == param_name:
                return constraint['value']
        return None


    def sample_subblock_params(self, subblock, sampling_fn):
        
        if subblock['category'] == 'layer':
            
            if 'repeats' in subblock:
                repeats = sampling_fn(subblock['repeats'], param_name=f'{subblock["name"]}_repeats')
                configs = []
                for i in range(repeats):
                    config = {}
                    for key in subblock:
                        if key == 'name':
                            config[key] = f"{subblock['name']}_{i+1}"
                        elif key in ['num_filters', 'kernel_size', 'num_units'] and type(subblock[key]) in [list, dict]:
                            c = self._check_constraints(subblock['name'], key)
                            if c is not None:
                                config[key] = c
                            else:
                                config[key] = sampling_fn(subblock[key], param_name=f'{subblock["name"]}_{key}')
                                if 'constraints' in subblock:
                                    self.constraints += [
                                        {
                                            **constraint,
                                            'value': config[key]
                                        } for constraint in subblock['constraints']
                                    ]
                        else:
                            config[key] = subblock[key]
                    del config['repeats']
                    configs.append(config)
                return configs
            
            else:
                config = {}
                for key in subblock:
                    if key in ['num_filters', 'kernel_size', 'num_units'] and type(subblock[key]) in [list, dict]:
                        c = self._check_constraints(subblock['name'], key)
                        if c is not None:
                            config[key] = c
                        else:
                            config[key] = sampling_fn(subblock[key], param_name=f'{subblock["name"]}_{key}')
                            if 'constraints' in subblock:
                                self.constraints += [
                                    {
                                        **constraint,
                                        'value': config[key]
                                    } for constraint in subblock['constraints']
                                ]
                    else:
                        config[key] = subblock[key]
                return config
        
        elif subblock['category'] == 'sequential':
            
            config = {}
            for key in subblock:
                if key != 'layers':
                    config[key] = subblock[key]
            
            config['layers'] = []
            for layer in subblock['layers']:
                subconfig = self.sample_subblock_params(layer, sampling_fn)
                if type(subconfig) == list:
                    config['layers'] += subconfig
                else:
                    config['layers'].append(subconfig)
            
            return config
        
        elif subblock['category'] == 'custom':

            if 'repeats' in subblock:

                repeats = sampling_fn(subblock['repeats'], param_name=f'{subblock["name"]}_repeats')
                configs = []

                for i in range(repeats):
                    
                    config = {}
                    for key in subblock:
                        if key != 'subblocks':
                            config[key] = subblock[key]
                    
                    config['subblocks'] = []
                    for sub_sb in subblock['subblocks']:
                        config['subblocks'].append(
                            self.sample_subblock_params(sub_sb, sampling_fn)
                        )
                    
                    self._adjust_repeated_names(config, i+1)

                    del config['repeats']
                    configs.append(config)

                return configs
            
            else:
            
                config = {}
                for key in subblock:
                    if key != 'subblocks':
                        config[key] = subblock[key]
                
                config['subblocks'] = []
                for sub_sb in subblock['subblocks']:
                    config['subblocks'].append(
                        self.sample_subblock_params(sub_sb, sampling_fn)
                    )
                
                return config

        else:
            raise Exception(f"Unsupported subblock category: {subblock['category']}")


    def _adjust_repeated_names(self, subblock, idx):

        subblock['name'] += f'_{idx}'
        
        if 'inputs' in subblock:
            if type(subblock['inputs']) != list:
                subblock['inputs'] += f'_{idx}'
            else:
                subblock['inputs'] = [i+f'_{idx}' for i in subblock['inputs']]
        
        if 'layers' in subblock:
            for layer in subblock['layers']:
                self._adjust_repeated_names(layer, idx)

        if 'subblocks' in subblock:
            for sub_sb in subblock['subblocks']:
                self._adjust_repeated_names(sub_sb, idx)


    def _get_layers_by_key(self, network_config, key, key_value):
        res = []
        for subblock_conf in network_config:
            res += self._get_layers_by_key_aux(subblock_conf, key, key_value)
        return res


    def _get_layers_by_key_aux(self, config, key, key_value):
        if config['category'] == 'layer' and key in config and config[key] == key_value:
            return [config]
        
        res = []
        
        if config['category'] == 'sequential':
            for layer_conf in config['layers']:
                res += self._get_layers_by_key_aux(layer_conf, key, key_value)
        
        if config['category'] == 'custom':
            for subblock_conf in config['subblocks']:
                res += self._get_layers_by_key_aux(subblock_conf, key, key_value)
        
        return res


    def _check_subblock_names(self, network_config):
        self.name_counter = Counter()
        for subblock_conf in network_config:
            self._count_names_aux(subblock_conf)
        for name in self.name_counter:
            if self.name_counter[name] > 1:
                print(f'[!] Non unique name in network: {name}')


    def _count_names_aux(self, config):
        self.name_counter.update([config['name']])
        
        if config['category'] == 'sequential':
            for layer_conf in config['layers']:
                self._count_names_aux(layer_conf)
        
        if config['category'] == 'custom':
            for subblock_conf in config['subblocks']:
                self._count_names_aux(subblock_conf)