# Generic Toolkit for Neural Architecture Search (GT-NAS)

This repository contains generic functionalities for performing Neural Architecture Search (NAS) with arbitrary models and datasets. Its underlying frameworks are TensorFlow and Optuna.

## JSON search space specification - grammar

This section of the README provides a semi-formal description on how a search space specification for neural architecture search experiments should be specified.

Search spaces should be defined as JSON documents of the form:

```
<search_space> ::= {
    "metadata": {
        "model_name": string_literal,
        ...some other pieces of matadata if needed...
    },
    "training_params": {
        // general hyperparameters related to training, e.g.
        "input_shape": list | <allowed_values>,
        "batch_size": <allowed_values>,
        "learning_rate": <allowed_values>,
        "optimizer": <allowed_values>,
        ...use-case specific parameters if needed...
    },
    "network_architecture": [list of <subblock> definitions]
}
```

The `<allowed_values>` term will be used throughout this document. It can be a list from which a parameter value can be taken, or a specification of a range of values, either floats or ints:

```
<allowed_values> ::= [list of values] | {"range": [a, b], "dtype": <dtype>}
<dtype> ::= "int" | "float"
```

The main part of the JSON file is the network architecture field, which should be a list of `<subblock>` specifications. Generally, this list serves only as a set of network parts that need to be linked together by specifying the inputs for each subblock. There are three possibilites for what a subblock can be: a layer, a sequence of layers, or a custom subblock:

```
<subblock> ::= <layer> | <sequential> | <custom>
```

A `<layer>` specification corresponds to a layer from `tf.keras.layers`. You can find explanations for the `<layer>` specification fields and the list of currently supported layers below:

```
<layer> ::= {
    "category": "layer",
    "name": string_literal,

    "inputs": "default" | [list of <layer>/<subblock> names]
    // default - use main model inputs

    "output": true | false
    // whether to include a layer's output in the list of final network outputs

    "repeats": <allowed_values>
    // how many times to repeat a layer in series
    // use only if the parent subblock is sequential

    "constraints": [list of <layer> names]
    // optional - list of layers that must have the same output shape as this one

    "type": "conv_2d" |
            "maxpool_2d" |
            "global_maxpool_2d" |
            "global_avgpool_2d" |
            "upsample_2d" |
            "deconv_2d" |
            "flatten" |
            "dense" |
            "add" |
            "concat" |
            "batch_norm" |
            "stop_grad" |
            "relu" |
            "identity" |
            "zeropad_2d"

    // layer-specific parameters

    "num_filters": <allowed_values>,    // conv_2d, deconv_2d
    "kernel_size": <allowed_values>,    // conv_2d, deconv_2d
    "num_units": <allowed_values>,      // dense
    "stride": int_literal,              // conv_2d, maxpool_2d, deconv_2d
    "padding": "valid" | "same",        // conv_2d, maxpool_2d, deconv_2d 
    "interpolation": "nearest" |
                     "bilinear" |
                     "bicubic" | ...    // upsample_2d
    "activation": "relu" |
                  "sigmoid" | ...       // conv_2d, deconv_2d, dense
    "axis": int_literal                 // concat, batch_norm
    "use_batch_norm": true | false      // any trainable layer
    "zeropad_spec": int | 
                    [int, int] | 
                    [[int, int],
                     [int, int]]        // zeropad_2d
}
```

A `<sequential>` specification corresponds to `tf.keras.Sequential`:

```
<sequential> ::= {
    "category": "sequential",
    "name": string_literal,

    "inputs": "default" | [list of <layer>/<subblock> names]
    // default - use main model inputs

    "output": true | false
    // whether to include sequential output in the list of final network outputs

    "layers": [list of <layer> definitions]
}
```

Having a way to specify `<custom>` subblocks enables parametrization of various non-sequential substructures commonly found in deep neural networks (e.g. residual blocks, inception blocks etc.). A `<custom>` subblock specification is recursive, it should contain a list of `<subblock>` definitions, which should be linked analogously to the root-level subblocks:

```
<custom> ::= {
    "category": "custom",
    "name": string_literal,

    "inputs": "default" | [list of <layer>/<subblock> names]
    // default - use main model inputs

    "output": true | false
    // whether to include a subblocks's output in the final network output

    "repeats": <allowed_values>
    // how many times to repeat a layer in series
    // use only if the parent subblock is sequential

    "subblocks": [list of <subblock> definitions]
    // must start with a <layer>
}
```

## Usage

Requirements (note that everything will probably work with some other versions as well, but these ones have been tested):

```
tensorflow == 2.13.0
optuna == 3.4.0
tqdm == 4.66.1
Flask == 3.0.0
```

Some examples of NAS experiments implemented through GT-NAS are given in the `experiments` folder. The steps needed to implement and run such an experiment are explained below.

Clone this repository:

```
git clone https://gitlab.com/pmf5/open-source/generic-toolkit-for-neural-architecture-search.git
```

Copy the `optimization.py` file into a directory where you will implement your experiments:

```
cp optimization.py <your_dir>
```

Insert the path to the local clone of GT-NAS repository into the `sys.path.append` call at the beginning of the copied `optimization.py` file.

Implement the missing, use-case specific parts of the `GenericNAS` class:
- dataset loading
- model training and evaluation

These parts are marked with TODO comments.

Start the benchmark server (either locally or on a different target device):

```
python3 benchmark.py -p <PORT>
```

In GT-NAS, we provide a rudimentary benchmark functionality as a starting point. Users can also implement their own benchmark code or extend the one provided here if needed.

Create a JSON search space specification conforming to the grammar explained above. Some examples can be found in the `search_space_examples` folder, as well as in the `experiments` folder.

Create an object of your `GenericNAS` class implementation and call the `optimize` function:

```
nas = GenericNAS(
    n_iterations=<int>,         # number of candidate architectures to examine
    search_strategy='optuna',
    search_space_json=<str>,    # path to a json search space specification
    dataset_path=<str>,         # path to a dataset
    out_dir=<str>,              # output directory to log results
    metrics_config=<dict>,      # a config dictionary, see examples in the experiments folder
    benchmark_url=<str>,        # url to a running benchmark server
    save_models=<True|False>,   # whether to save models with trained weights
    storage_limit=<int|None>    # how many results to save on disk (None for unlimited)
)

nas.optimize()
```

For more information, feel free to send an email to nikola.pizurica.1998@gmail.com