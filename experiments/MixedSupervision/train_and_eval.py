import tensorflow as tf
import tqdm


def compute_base_score(test_set, model):
    pr_auc_clf = tf.keras.metrics.AUC(curve='PR')
    pr_auc_seg = tf.keras.metrics.AUC(curve='PR')

    for img, mask, _, lbl in test_set:
        Sh, Cp = model(img, training=False)

        Sh = tf.sigmoid(Sh)
        Cp = tf.sigmoid(Cp)

        pr_auc_clf.update_state(lbl, Cp)
        pr_auc_seg.update_state(mask, Sh)

    print(f'AP_clf: {float(pr_auc_clf.result().numpy())}, AP_seg: {float(pr_auc_seg.result().numpy())}')

    return float(pr_auc_clf.result().numpy()) + float(pr_auc_seg.result().numpy())


def compute_loss(model, img, mask, segw, lbl, gamma, delta, Lambda):
    Sh, Cp = model(img)
    Lseg = tf.reduce_sum(
        segw * tf.nn.sigmoid_cross_entropy_with_logits(logits=Sh, labels=mask),
        axis=[1, 2]
    )
    Lclf = tf.nn.sigmoid_cross_entropy_with_logits(logits=Cp, labels=lbl)
    return tf.reduce_mean(Lambda*gamma*Lseg) + tf.reduce_mean((1 - Lambda)*delta*Lclf)


def train_step(model, optimizer, img, mask, segw, lbl, gamma, delta, Lambda):
    with tf.GradientTape() as tape:
        loss = compute_loss(model, img, mask, segw, lbl, gamma, delta, Lambda)
    grad = tape.gradient(loss, model.trainable_variables)
    optimizer.apply_gradients(zip(grad, model.trainable_variables))


def train_loop(train_pos, train_neg_iter, test, model, optimizer, epochs, delta):
    for epoch in tqdm.tqdm(range(1, epochs + 1)):
        Lambda = 1.0 - epoch/epochs
        for img, mask, segw, lbl, gamma in train_pos:
            train_step(model, optimizer, img, mask, segw, lbl, gamma, delta, Lambda)
            _img, _mask, _segw, _lbl, _gamma = train_neg_iter.get_next()
            train_step(model, optimizer, _img, _mask, _segw, _lbl, _gamma, delta, Lambda)
    return compute_base_score(test, model)


def kd_train_step(teacher_model, student_model, optimizer, img, mask, segw, lbl, gamma, delta, Lambda, 
                  distill_loss_fn, temperature, alpha):
    
    teacher_sh, teacher_cp = teacher_model(img)
    
    with tf.GradientTape() as tape:
        student_sh, student_cp = student_model(img)

        Lseg = tf.reduce_sum(
            segw * tf.nn.sigmoid_cross_entropy_with_logits(logits=student_sh, labels=mask),
            axis=[1, 2]
        )
        Lclf = tf.nn.sigmoid_cross_entropy_with_logits(logits=student_cp, labels=lbl)

        seg_loss, clf_loss = tf.reduce_mean(Lseg), tf.reduce_mean(Lclf)

        clf_distill = distill_loss_fn(
            tf.sigmoid(teacher_cp / temperature),
            tf.sigmoid(student_cp / temperature),
        )

        seg_distill = distill_loss_fn(
            tf.sigmoid(teacher_sh / temperature),
            tf.sigmoid(student_sh / temperature)
        )

        loss = Lambda*gamma*(alpha*seg_loss + (1 - alpha)*seg_distill) + (1 - Lambda)*delta*(alpha*clf_loss + (1 - alpha)*clf_distill)

    grad = tape.gradient(loss, student_model.trainable_variables)
    optimizer.apply_gradients(zip(grad, student_model.trainable_variables))


def kd_train_loop(train_pos, train_neg_iter, test, teacher_model, student_model,
                  optimizer, epochs, delta, temperature, alpha):
    
    distill_loss_fn = tf.keras.losses.KLDivergence()

    for epoch in tqdm.tqdm(range(1, epochs + 1)):
        Lambda = 1.0 - epoch/epochs
        for img, mask, segw, lbl, gamma in train_pos:
            kd_train_step(
                teacher_model, student_model,
                optimizer, img, mask, segw, lbl, gamma, delta, Lambda,
                distill_loss_fn, temperature, alpha
            )
            _img, _mask, _segw, _lbl, _gamma = train_neg_iter.get_next()
            kd_train_step(
                teacher_model, student_model,
                optimizer, _img, _mask, _segw, _lbl, _gamma, delta, Lambda,
                distill_loss_fn, temperature, alpha
            )

    return compute_base_score(test, student_model)

