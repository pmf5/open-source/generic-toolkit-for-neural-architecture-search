import tensorflow as tf
gpus = tf.config.list_physical_devices('GPU')
print(gpus)
for gpu in gpus:
    tf.config.experimental.set_memory_growth(gpu, True)

from optimization import Generic_NAS


nas = Generic_NAS(
    n_iterations=100,
    search_strategy='optuna',
    search_space_json='ms_ksdd2_search_space.json',
    dataset_path='KolektorSDD2',
    out_dir='ms_ksdd2_opt',
    metrics_config={
        'extra': [
            {'id': 'LATENCY', 'weight': -1}
        ]
    },
    benchmark_url='http://localhost:8888/benchmark/cpu',
    save_models=True,
    storage_limit=10
)

nas.optimize()
