import tensorflow as tf
import tensorflow_addons as tfa
import os


DATALOADER_PARAMS = {
    'height': 640,
    'width': 232,
    'resize_factor': 8,
    'dil_ksize': 15,
    'mixed_sup_N': 246,
    'dist_trans_w': 3.0,
    'dist_trans_p': 2.0,
    'shuffle_buf_size': 3500,
    'batch_size': 4
}


def set_loader_params(dil_ksize, dist_trans_w, dist_trans_p, batch_size):
    DATALOADER_PARAMS['dil_ksize'] = dil_ksize
    DATALOADER_PARAMS['dist_trans_w'] = dist_trans_w
    DATALOADER_PARAMS['dist_trans_p'] = dist_trans_p
    DATALOADER_PARAMS['batch_size'] = batch_size


def parse_element(e):
    img_path = e[0]
    img = tf.io.read_file(img_path)
    img = tf.image.decode_png(img, channels=3)
    img = tf.image.convert_image_dtype(img, dtype=tf.float32)
    img = tf.image.resize(img, (DATALOADER_PARAMS['height'], DATALOADER_PARAMS['width']))
    
    mask_path = e[1]
    mask = tf.io.read_file(mask_path)
    mask = tf.image.decode_png(mask, channels=1)
    mask = tf.image.convert_image_dtype(mask, dtype=tf.float32)
    mask = tf.image.resize(mask, (DATALOADER_PARAMS['height'], DATALOADER_PARAMS['width']))
    mask = tf.where(mask>0.0, x=1.0, y=0.0)
    lbl = tf.reshape(tf.reduce_max(mask), shape=(1,))

    dil = tf.nn.dilation2d(
        tf.reshape(mask, (1, tf.shape(mask)[0], tf.shape(mask)[1], 1)),
        filters=tf.zeros((DATALOADER_PARAMS['dil_ksize'], DATALOADER_PARAMS['dil_ksize'], 1)),
        strides=(1, 1, 1, 1),
        dilations=(1, 1, 1, 1),
        padding='SAME',
        data_format='NHWC'
    )

    def segw_pos():
        edt = tfa.image.euclidean_dist_transform(
            tf.image.convert_image_dtype(dil[0,:,:,:], dtype=tf.uint8)
        )
        edt = edt / tf.reduce_max(edt)
        edt = edt ** DATALOADER_PARAMS['dist_trans_p']
        edt = edt * DATALOADER_PARAMS['dist_trans_w']
        edt = tf.where(edt==0.0, x=1.0, y=edt)
        return edt
    
    def segw_neg():
        return mask + 1.0

    segw = tf.cond(tf.greater(tf.reduce_max(mask), 0.0), true_fn=segw_pos, false_fn=segw_neg)
    
    mask = tf.image.resize(
        mask, 
        (
            tf.shape(mask)[0] // DATALOADER_PARAMS['resize_factor'],
            tf.shape(mask)[1] // DATALOADER_PARAMS['resize_factor']
        )
    )
    
    segw = tf.image.resize(
        segw,
        (
            tf.shape(segw)[0] // DATALOADER_PARAMS['resize_factor'],
            tf.shape(segw)[1] // DATALOADER_PARAMS['resize_factor']
        )
    )

    return img, mask, segw, lbl

    
_n = tf.Variable(0, dtype=tf.int32)
_N = tf.Variable(DATALOADER_PARAMS['mixed_sup_N'], dtype=tf.int32)

def take_N(img, mask, segw, lbl, n, N):
    def _true_fn():
        n.assign_add(1)
        return 1.0
    def _false_fn():
        return 0.0
    gamma = tf.cond(tf.less(n, N), true_fn=_true_fn, false_fn=_false_fn)
    return img, mask, segw, lbl, gamma


def load_ksdd2(dataset_path):
    train_paths = [
        (
            os.path.join(dataset_path, 'train', filename),
            os.path.join(dataset_path, 'train', filename.replace('.png', '_GT.png'))
        )
        for filename in os.listdir(os.path.join(dataset_path, 'train')) if not filename.endswith('_GT.png')
    ]
    test_paths = [
        (
            os.path.join(dataset_path, 'test', filename),
            os.path.join(dataset_path, 'test', filename.replace('.png', '_GT.png'))
        )
        for filename in os.listdir(os.path.join(dataset_path, 'test')) if not filename.endswith('_GT.png')
    ]

    train = tf.data.Dataset.from_tensor_slices(train_paths)
    test = tf.data.Dataset.from_tensor_slices(test_paths)

    train = train.map(parse_element)
    train_neg = train.filter(lambda img, mask, segw, lbl: tf.reduce_max(lbl) == 0.0)
    train_neg = train_neg.map(lambda img, mask, segw, lbl: (img, mask, segw, lbl, 1.0))
    train_neg = train_neg \
        .shuffle(DATALOADER_PARAMS['shuffle_buf_size']) \
        .batch(DATALOADER_PARAMS['batch_size']) \
        .cache().repeat().prefetch(1)
    train_neg_iter = iter(train_neg)
    train_pos = train.filter(lambda img, mask, segw, lbl: tf.reduce_max(lbl) == 1.0)
    train_pos = train_pos.map(lambda img, mask, segw, lbl: take_N(img, mask, segw, lbl, _n, _N))
    train_pos = train_pos \
        .shuffle(DATALOADER_PARAMS['shuffle_buf_size']) \
        .batch(DATALOADER_PARAMS['batch_size']) \
        .cache().prefetch(1)

    test = test.map(parse_element)
    test = test.batch(1).cache().prefetch(1)

    return train_pos, train_neg_iter, test