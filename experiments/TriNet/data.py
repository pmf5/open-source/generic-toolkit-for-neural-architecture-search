import tensorflow as tf
import random
import os


class SensumSODF_DataLoader:
    
    def __init__(self, dataset_path, apply_aug=True, batch_size=1, cache=None):
        
        self.dataset_path = dataset_path
        
        self.apply_aug = apply_aug
        self.batch_size = batch_size
        
        # None - no caching
        # 'RAM' - RAM caching
        # '<path_on_disk>' - caching on the local disk
        self.cache = cache

    
    def load_kfold(self, k):

        pos_paths = [(
            os.path.join(self.dataset_path, 'positive/data', filename),
            os.path.join(self.dataset_path, 'positive/annotation', filename)
        ) for filename in os.listdir(os.path.join(self.dataset_path, 'positive/data'))]

        random.shuffle(pos_paths)

        np = len(pos_paths)
        pos_path_folds = [pos_paths[i*np//k : (i+1)*np//k] for i in range(k)]
        
        if self.apply_aug:

            def _load_pos(path):
                
                img_path = path[0]
                mask_path = path[1]
    
                img = tf.io.read_file(img_path)
                img = tf.image.decode_png(img, channels=3)
                img = tf.image.convert_image_dtype(img, dtype=tf.float32)

                img_batch = tf.stack([
                    img,
                    tf.image.rot90(img, k=2),
                    tf.image.flip_left_right(img),
                    tf.image.flip_up_down(img)
                ], axis=0)
    
                mask = tf.io.read_file(mask_path)
                mask = tf.image.decode_png(mask, channels=1)
                mask = tf.image.convert_image_dtype(mask, dtype=tf.float32)

                mask_batch = tf.stack([
                    mask,
                    tf.image.rot90(mask, k=2),
                    tf.image.flip_left_right(mask),
                    tf.image.flip_up_down(mask)
                ], axis=0)

                lbl = tf.reduce_max(mask)
                lbl_batch = tf.stack([lbl]*4, axis=0)
    
                return img_batch, mask_batch, lbl_batch

            pos_folds = [
                tf.data.Dataset.from_tensor_slices(
                    pos_path_fold
                ).map(
                    _load_pos,
                    num_parallel_calls=tf.data.AUTOTUNE
                ).unbatch() for pos_path_fold in pos_path_folds
            ]

        else:
            
            def _load_pos(path):
                
                img_path = path[0]
                mask_path = path[1]
    
                img = tf.io.read_file(img_path)
                img = tf.image.decode_png(img, channels=3)
                img = tf.image.convert_image_dtype(img, dtype=tf.float32)
    
                mask = tf.io.read_file(mask_path)
                mask = tf.image.decode_png(mask, channels=1)
                mask = tf.image.convert_image_dtype(mask, dtype=tf.float32)

                lbl = tf.reduce_max(mask)
    
                return img, mask, lbl

            pos_folds = [
                tf.data.Dataset.from_tensor_slices(
                    pos_path_fold
                ).map(
                    _load_pos,
                    num_parallel_calls=tf.data.AUTOTUNE
                ) for pos_path_fold in pos_path_folds
            ]

        neg_paths = [(
            os.path.join(self.dataset_path, 'negative/data', filename)
        ) for filename in os.listdir(os.path.join(self.dataset_path, 'negative/data'))]

        random.shuffle(neg_paths)

        nn = len(neg_paths)
        neg_path_folds = [neg_paths[i*nn//k : (i+1)*nn//k] for i in range(k)]

        def _load_neg(img_path):
            
            img = tf.io.read_file(img_path)
            img = tf.image.decode_png(img, channels=3)
            img = tf.image.convert_image_dtype(img, dtype=tf.float32)

            mask = tf.zeros(tf.shape(img)[:2], dtype=tf.float32)
            mask = tf.expand_dims(mask, axis=-1)
            
            lbl = tf.constant(0.0, dtype=tf.float32)

            return img, mask, lbl

        neg_folds = [
            tf.data.Dataset.from_tensor_slices(
                neg_path_fold
            ).map(
                _load_neg,
                num_parallel_calls=tf.data.AUTOTUNE
            ) for neg_path_fold in neg_path_folds
        ]

        dataset_folds = [pos_fold.concatenate(neg_fold) for pos_fold, neg_fold in zip(pos_folds, neg_folds)]
        if self.cache is not None:
            if self.cache == 'RAM':
                dataset_folds = [fold.cache() for fold in dataset_folds]
            else:
                raise Exception('Caching on a local disk is not supported for the SensumSODF dataset')
        dataset_folds = [
            fold.shuffle(1000).batch(self.batch_size).prefetch(tf.data.AUTOTUNE) for fold in dataset_folds
        ]

        return dataset_folds

