import tensorflow as tf
import tqdm
import random


class TriNet_SODF:

    def __init__(self, model, dataset_folds, mask_weights, loss_fn, optimizer):
        
        self.model = model
        self.initial_model_weights = model.get_weights()
        self.dataset_folds = dataset_folds
        self.mask_weights = mask_weights
        self.loss_fn = loss_fn
        self.optimizer = optimizer

        def seg_loss(img, mask):
            mask_preds = self.model(img)[:5]
            mask_preds = tf.concat(mask_preds, axis=0)
            mask_gts = tf.concat([mask]*5, axis=0)
            return self.loss_fn(mask_gts, mask_preds, sample_weight=self.mask_weights)
        self.seg_loss = seg_loss
        
        def clf_loss(img, lbl):
            pred = self.model(img)[5]
            return self.loss_fn(lbl, pred)
        self.clf_loss = clf_loss
        
        @tf.function
        def seg_train_step(img, mask):
            with tf.GradientTape() as tape:
                loss = self.seg_loss(img, mask)
            grad = tape.gradient(loss, self.model.trainable_variables)
            optimizer.apply_gradients(zip(grad, self.model.trainable_variables))
        self.seg_train_step = seg_train_step
        
        @tf.function
        def clf_train_step(img, lbl):
            with tf.GradientTape() as tape:
                loss = self.clf_loss(img, lbl)
            grad = tape.gradient(loss, self.model.trainable_variables)
            optimizer.apply_gradients(zip(grad, self.model.trainable_variables))
        self.clf_train_step = clf_train_step

    
    def cross_validate(self, seg_epochs, clf_epochs):

        k = len(self.dataset_folds)
        mean_score = 0.0

        for i in range(k):

            print(i)
            
            test = self.dataset_folds[i]
            
            train_folds = self.dataset_folds[:i] + self.dataset_folds[i+1:]
            train = train_folds[0]
            for fold in train_folds[1:]:
                train = train.concatenate(fold)

            self.model.set_weights(self.initial_model_weights)

            for j in tqdm.tqdm(range(seg_epochs)):
                for img, mask, lbl in train:
                    self.seg_train_step(img, mask)
            for j in tqdm.tqdm(range(clf_epochs)):
                for img, mask, lbl in train:
                    self.clf_train_step(img, lbl)

            roc_auc = tf.keras.metrics.AUC(num_thresholds=200, curve='ROC')
            for img, mask, lbl in test:
                roc_auc.update_state(lbl, self.model(img)[5])
            mean_score += roc_auc.result().numpy() / k

        return float(mean_score)


    def train_and_test(self, seg_epochs, clf_epochs):

        k = len(self.dataset_folds)
        test_idx = random.randint(0, k-1)

        test = self.dataset_folds[test_idx]

        train_folds = self.dataset_folds[:test_idx] + self.dataset_folds[test_idx+1:]
        train = train_folds[0]
        for fold in train_folds[1:]:
            train = train.concatenate(fold)

        self.model.set_weights(self.initial_model_weights)

        for j in tqdm.tqdm(range(seg_epochs)):
            for img, mask, lbl in train:
                self.seg_train_step(img, mask)
        for j in tqdm.tqdm(range(clf_epochs)):
            for img, mask, lbl in train:
                self.clf_train_step(img, lbl)

        roc_auc = tf.keras.metrics.AUC(num_thresholds=200, curve='ROC')

        for img, mask, lbl in test:
            roc_auc.update_state(lbl, self.model(img)[5])
        
        return float(roc_auc.result().numpy())