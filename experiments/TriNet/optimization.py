import tensorflow as tf
import optuna
import json
import os
import shutil
from pathlib import Path

import sys
import inspect

sys.path.append(
    os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(inspect.getfile(inspect.currentframe())))))
)

from sampling import ParamSampler
from parsing import ModelFactory
from utils import calibrate, call_benchmark

from data import SensumSODF_DataLoader
from train_and_eval import TriNet_SODF


class Generic_NAS:

    def __init__(self,
                 n_iterations,
                 search_strategy,
                 search_space_json,
                 dataset_path,
                 out_dir,
                 metrics_config,
                 benchmark_url,
                 save_models=False,
                 storage_limit=None):
        
        self.n_iterations = n_iterations
        self.search_strategy = search_strategy

        self.param_sampler = ParamSampler(self.search_strategy)
        self.model_factory = ModelFactory()
        
        with open(search_space_json, 'r') as f:
            self.search_space = json.load(f)
        
        self.dataset_path = dataset_path
        self.out_dir = out_dir
        self.metrics_config = metrics_config
        self.benchmark_url = benchmark_url

        self.metrics_calib = calibrate(
            search_space=self.search_space,
            benchmark_url=self.benchmark_url,
            metrics=[mc['id'] for mc in self.metrics_config['extra']]
        )

        self.log = {
            'metrics_config': self.metrics_config,
            'metrics_calib': self.metrics_calib,
            'score_history': []
        }

        self.save_models = save_models
        
        self.storage_limit = storage_limit
        if self.storage_limit is not None:
            self.leaderboard = []
        
        Path(self.out_dir).mkdir(parents=True, exist_ok=True)
    

    def total_score(self, base_score, extra_metrics):

        tot = base_score
        penalty = 1

        if 'base' in self.metrics_config:
            if 'lower_thresh' in self.metrics_config['base'] and base_score < self.metrics_config['base']['lower_thresh']:
                tot -= penalty
            elif 'upper_thresh' in self.metrics_config['base'] and base_score > self.metrics_config['base']['upper_thresh']:
                tot -= penalty
        
        for mc in self.metrics_config['extra']:
            tot += extra_metrics[mc['id']] * mc['weight'] / self.metrics_calib[mc['id']]
            if 'lower_thresh' in mc and extra_metrics[mc['id']] < mc['lower_thresh']:
                tot -= penalty
            elif 'upper_thresh' in mc and extra_metrics[mc['id']] > mc['upper_thresh']:
                tot -= penalty
                
        return tot


    def _update(self, id, config, base_score, extra_metrics, model):

        self.log['score_history'].append({
            'id': id,
            'base_score': base_score,
            'extra_metrics': extra_metrics
        })

        with open(os.path.join(self.out_dir, 'log.json'), 'w') as f:
            json.dump(self.log, f, indent=2)
        
        Path(os.path.join(self.out_dir, str(id))).mkdir(parents=True, exist_ok=True)
        with open(os.path.join(self.out_dir, str(id), 'config.json'), 'w') as f:
            json.dump(config, f, indent=2)
        with open(os.path.join(self.out_dir, str(id), 'score.json'), 'w') as f:
            json.dump({
                'base_score': base_score,
                'extra_metrics': extra_metrics
            }, f, indent=2)

        if self.save_models:
            model.save(os.path.join(self.out_dir, str(id), 'model.h5'))

        if self.storage_limit is not None:

            self.leaderboard.append({
                'id': id,
                'base_score': base_score,
                'extra_metrics': extra_metrics
            })

            self.leaderboard.sort(
                key=lambda x: self.total_score(x['base_score'], x['extra_metrics']),
                reverse=True
            )

            for x in self.leaderboard[self.storage_limit:]:
                shutil.rmtree(os.path.join(self.out_dir, str(x['id'])))
            
            while len(self.leaderboard) > self.storage_limit:
                self.leaderboard.pop()


    def optimize(self):

        def optuna_objective(trial):

            config = self.param_sampler.sample(self.search_space, trial=trial)

            model = self.model_factory.create_model_from_config(
                config['network_architecture'],
                config['training_params']['input_shape'],
                config['metadata']['model_name']
            )
            model.summary()

            # ===================================================================
            dataloader = SensumSODF_DataLoader(
                dataset_path=self.dataset_path,
                apply_aug=True,
                batch_size=config['training_params']['batch_size'],
                cache='RAM'
            )
            dataset_folds = dataloader.load_kfold(3)
            # ===================================================================

            opt = config['training_params']['optimizer']
            lr = config['training_params']['learning_rate']

            if opt == 'adam':
                optimizer = tf.keras.optimizers.Adam(lr)
            elif opt == 'nadam':
                optimizer = tf.keras.optimizers.Nadam(lr)
            elif opt == 'rmsprop':
                optimizer = tf.keras.optimizers.RMSprop(lr)
            else:
                raise Exception(f'Unsupported optimizer: {opt}')

            # ===================================================================
            trinet_sodf = TriNet_SODF(
                model=model,
                dataset_folds=dataset_folds,
                mask_weights=tf.ones_like((5,), dtype=tf.float32),
                loss_fn=tf.keras.losses.BinaryCrossentropy(),
                optimizer=optimizer
            )

            base_score = trinet_sodf.train_and_test(
                seg_epochs=config['training_params']['seg_epochs'],
                clf_epochs=config['training_params']['clf_epochs']
            )
            # ===================================================================

            extra_metrics = call_benchmark(
                model=model,
                benchmark_url=self.benchmark_url,
                metrics=[mc['id'] for mc in self.metrics_config['extra']]
            )

            self._update(trial.number, config, base_score, extra_metrics, model)

            return self.total_score(base_score, extra_metrics)

        study = optuna.create_study(direction="maximize")
        study.optimize(optuna_objective, n_trials=self.n_iterations)

        with open(os.path.join(self.out_dir, 'log.json'), 'w') as f:
            json.dump(self.log, f, indent=2)