import tensorflow as tf
gpus = tf.config.list_physical_devices('GPU')
print(gpus)
for gpu in gpus:
    tf.config.experimental.set_memory_growth(gpu, True)

from optimization import Generic_NAS

nas = Generic_NAS(
    n_iterations=100,
    search_strategy='optuna',
    search_space_json='trinet_capsule_search_space.json',
    dataset_path='SensumSODF/capsule',
    out_dir='trinet_capsule_opt',
    metrics_config={
        'base': {
            'lower_thresh': 0.96
        },
        'extra': [
            {'id': '#PARAMS', 'weight': -1}
        ]
    },
    benchmark_url='http://localhost:8888/benchmark/gpu',
    save_models=False,
    storage_limit=None
)

nas.optimize()
