import tensorflow as tf
from tensorflow.python.keras.utils.layer_utils import count_params
gpus = tf.config.list_physical_devices('GPU')
print(gpus)
for gpu in gpus:
    tf.config.experimental.set_memory_growth(gpu, True)

import argparse
import os
import time

from flask import Flask, request, jsonify


app = Flask(__name__)


def cli():
    parser = argparse.ArgumentParser(description='A simple benchmark server')
    parser.add_argument('--port', '-p', required=True, type=int, help='Server port')
    return parser


@app.route('/benchmark/<device>', methods=['POST'])
def baas_http(device):
    try:
        model_file = request.files['model']
        model_file.save('tmp_model.h5')

        model = tf.keras.models.load_model('tmp_model.h5')
        input_size = model.input_shape
        if input_size[0] is None:
            input_size = (1,) + input_size[1:]

        # warm up
        for i in range(50):
            input_data = tf.random.uniform(input_size, minval=0.0, maxval=1.0)
            if device == "gpu":
                with tf.device("gpu:0"):
                    model_output = model(input_data, training=False)
            else:
                with tf.device("cpu:0"):
                    model_output = model(input_data, training=False)
        
        n = 50
        avg_infer_time = 0.0

        # actual benchmark
        for i in range(n):
            input_data = tf.random.uniform(input_size, minval=0.0, maxval=1.0)

            t0 = time.perf_counter()
            if device == "gpu":
                with tf.device("gpu:0"):
                    model_output = model(input_data, training=False)
            else:
                with tf.device("cpu:0"):
                    model_output = model(input_data, training=False)
            avg_infer_time += (time.perf_counter() - t0) / n
        
        response = {
            '#PARAMS': count_params(model.trainable_weights),
            'LATENCY': avg_infer_time
        }

        os.remove('tmp_model.h5')

        return jsonify(response)

    except Exception as e:
        for f in os.listdir('tmp'):
            os.remove('tmp_model.h5')
        return str(e), 400


def main():
    args = cli().parse_args()
    app.run(host='0.0.0.0', port=int(args.port), debug=True)


if __name__ == "__main__":
    main()