import tensorflow as tf


class ModelFactory:
    
    def __init__(self):
        pass


    def create_model_from_config(self, network_config, input_shape, model_name):
        self.input_shape = input_shape
        self.input = tf.keras.Input(shape=self.input_shape)

        # keeping all created subblocks in memory
        self.subblocks = {}

        # intermediate layer outputs
        self.temp_outputs = {}

        # final network outputs
        self.outputs = []

        for subblock_conf in network_config:
            self._parse_subblock_config(subblock_conf)
        
        return tf.keras.Model(inputs=self.input, outputs=self.outputs, name=model_name)


    def _parse_subblock_config(self, subblock_conf, parent_conf=None):
        
        if subblock_conf['category'] == 'layer':
            
            layer = self._create_layer(subblock_conf)
            self.subblocks[subblock_conf['name']] = layer
            self._link_subblock(layer, subblock_conf)
            return layer
        
        elif subblock_conf['category'] == 'sequential':
            
            layers = []
            for layer_conf in subblock_conf['layers']:
                if layer_conf['category'] == 'layer':
                    layer = self._create_layer(layer_conf)
                    layers.append(layer)
                    if 'use_batch_norm' in layer_conf and layer_conf['use_batch_norm'] == True:
                        layers.append(tf.keras.layers.BatchNormalization())
                else:
                    layer = self._parse_subblock_config(layer_conf, parent_conf=subblock_conf)
                    layers.append(layer)
                    if 'use_batch_norm' in layer_conf and layer_conf['use_batch_norm'] == True:
                        layers.append(tf.keras.layers.BatchNormalization())

            seq = tf.keras.Sequential(layers, name=subblock_conf['name'])
            self.subblocks[subblock_conf['name']] = seq
            self._link_subblock(seq, subblock_conf)
            return seq
        
        elif subblock_conf['category'] == 'custom':
            
            if parent_conf is not None and parent_conf['category'] == 'sequential':

                prev_out_shape = self._get_prev_out_shape(subblock_conf, parent_conf)

                if prev_out_shape is None:
                    if parent_conf['inputs'] == 'default':
                        _in = tf.keras.Input(shape=self.input_shape)
                    else:
                        _in = tf.keras.Input(
                            shape=self.temp_outputs[parent_conf['inputs']].shape[1:]
                        )
                else:
                    _in = tf.keras.Input(shape=prev_out_shape)
                
                first_layer_conf = subblock_conf['subblocks'][0]
                first_layer = self._create_layer(first_layer_conf)
                self.subblocks[first_layer_conf['name']] = first_layer
                self._link_subblock(first_layer, first_layer_conf, spec_input=_in)
                
                for ssb_conf in subblock_conf['subblocks'][1:]:
                    ssb = self._parse_subblock_config(ssb_conf)
                    self._link_subblock(ssb, ssb_conf)
                
                submodel = tf.keras.Model(
                    inputs=_in,
                    outputs=self.temp_outputs[subblock_conf['subblocks'][-1]['name']],
                    name=subblock_conf['name']
                )
                self.subblocks[subblock_conf['name']] = submodel
                
                return submodel

            else:

                if subblock_conf['inputs'] == 'default':
                    _in = self.input
                else:
                    if type(subblock_conf['inputs']) == list:
                        _in = [self.temp_outputs[i] for i in subblock_conf['inputs']]
                    else:
                        _in = self.temp_outputs[subblock_conf['inputs']]

                for ssb_conf in subblock_conf['subblocks']:
                    ssb = self._parse_subblock_config(ssb_conf)
                    self._link_subblock(ssb, ssb_conf)
                
                submodel = tf.keras.Model(
                    inputs=_in,
                    outputs=self.temp_outputs[subblock_conf['subblocks'][-1]['name']],
                    name=subblock_conf['name']
                )
                self.subblocks[subblock_conf['name']] = submodel
                
                return submodel

        else:
            raise Exception(f"Unsupported subblock category: {subblock_conf['category']}")
    

    def _create_layer(self, layer_config):
        
        # trainable
        # =========

        if layer_config['type'] == 'conv_2d':
            return tf.keras.layers.Conv2D(
                filters=layer_config['num_filters'],
                kernel_size=layer_config['kernel_size'],
                strides=layer_config['stride'],
                padding=layer_config['padding'],
                activation=layer_config['activation'],
                name=layer_config['name']
            )
        
        if layer_config['type'] == 'deconv_2d':
            return tf.keras.layers.Conv2DTranspose(
                filters=layer_config['num_filters'],
                kernel_size=layer_config['kernel_size'],
                strides=layer_config['stride'],
                padding=layer_config['padding'],
                activation=layer_config['activation'],
                name=layer_config['name']
            )
        
        elif layer_config['type'] == 'dense':
            return tf.keras.layers.Dense(
                units=layer_config['num_units'],
                activation=layer_config['activation'],
                name=layer_config['name']
            )
        
        # pooling
        # =======
        
        elif layer_config['type'] == 'maxpool_2d':
            return tf.keras.layers.MaxPooling2D(
                pool_size=layer_config['pool_size'],
                strides=layer_config['stride'],
                padding=layer_config['padding'],
                name=layer_config['name']
            )

        elif layer_config['type'] == 'avgpool_2d':
            return tf.keras.layers.AveragePooling2D(
                pool_size=layer_config['pool_size'],
                strides=layer_config['stride'],
                padding=layer_config['padding'],
                name=layer_config['name']
            )
        
        elif layer_config['type'] == 'global_maxpool_2d':
            return tf.keras.layers.GlobalMaxPooling2D(name=layer_config['name'])
        
        elif layer_config['type'] == 'global_avgpool_2d':
            return tf.keras.layers.GlobalAveragePooling2D(name=layer_config['name'])
        
        # expanding
        # =========
        
        elif layer_config['type'] == 'upsample_2d':
            return tf.keras.layers.UpSampling2D(
                size=layer_config['size'],
                interpolation=layer_config['interpolation'],
                name=layer_config['name']
            )
        
        elif layer_config['type'] == 'zeropad_2d':
            return tf.keras.layers.ZeroPadding2D(
                padding=layer_config['zeropad_spec'],
                name=layer_config['name']
            )
        
        # merging
        # =======

        elif layer_config['type'] == 'concat':
            return tf.keras.layers.Concatenate(
                axis=layer_config['axis'],
                name=layer_config['name']
            )
        
        elif layer_config['type'] == 'add':
            return tf.keras.layers.Add(name=layer_config['name'])

        # Misc
        # =======

        elif layer_config['type'] == 'relu':
            return tf.keras.layers.ReLU(name=layer_config['name'])
        
        elif layer_config['type'] == 'batch_norm':
            return tf.keras.layers.BatchNormalization(name=layer_config['name'])
        
        elif layer_config['type'] == 'stop_grad':
            return tf.keras.layers.Lambda(
                lambda x: tf.stop_gradient(x),
                name=layer_config['name']
            )
        
        elif layer_config['type'] == 'identity':
            return tf.keras.layers.Identity(name=layer_config['name'])
        
        else:
            raise Exception(f"Unsupported layer type: {layer_config['type']}")
    

    def _link_subblock(self, subblock, subblock_conf, spec_input=None):
        
        if spec_input is not None:
            _in = spec_input
            _out = subblock(_in)
        
        elif subblock_conf['inputs'] == 'default':
            _in = self.input
            _out = subblock(_in)
        
        else:
            if type(subblock_conf['inputs']) == list:
                _in = [self.temp_outputs[i] for i in subblock_conf['inputs']]
                _out = subblock(_in)
            else:
                _in = self.temp_outputs[subblock_conf['inputs']]
                _out = subblock(_in)
        
        self.temp_outputs[subblock_conf['name']] = _out
        if 'output' in subblock_conf and subblock_conf['output'] == True:
            self.outputs.append(_out)
    

    def _get_prev_out_shape(self, sb_conf, seq_par_conf):
        
        prev = -1
        for curr in range(len(seq_par_conf['layers'])):
            if sb_conf['name'] == seq_par_conf['layers'][curr]['name']:
                break
            prev += 1
        
        if prev == -1:
            return None
        
        else:
            prev_sb = seq_par_conf['layers'][prev]
            if prev_sb['category'] == 'layer':
                return self.temp_outputs[prev_sb['name']].shape[1:]
            elif prev_sb['category'] == 'custom':
                return self.temp_outputs[prev_sb['subblocks'][-1]['name']].shape[1:]
            else:
                raise Exception(f'Unsupported nesting of sequential subblock: {prev_sb}')